import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Marcos on 31/03/17.
 */
public class Atividade1 {

    //create pallete
    private String colors[] = {  "#000000", "#0000AA", "#00AA00", "#00AAAA", "#AA0000", "#AA00AA", "#AAAA00", "#AAAAAA", "#000055", "#0000FF", "#00AA55", "#00AAFF", "#AA0055", "#AA00FF", "#AAAA55", "#AAAAFF",

            "#005500", "#0055AA", "#00FF00", "#00FFAA", "#AA5500", "#AA55AA", "#AAFF00", "#AAFFAA", "#005555", "#0055FF", "#00FF55", "#00FFFF",  "#AA5555", "#AA55FF",  "#AAFF55", "#AAFFFF",

            "#550000",  "#5500AA",  "#55AA00", "#55AAAA", "#FF0000",  "#FF00AA",  "#FFAA00",  "#FFAA00",  "#FFAAAA",  "#550055",  "#5500FF",  "#55AA55",  "#55AAFF",  "#FF0055",  "#FF00FF",  "#FFAA55", "#FFAAFF",

            "#555500",  "#5555AA",  "#55FF00",  "#55FFAA",  "#FF5500",  "#FF55AA",  "#FFFF00",  "#FFFFAA",  "#555555",  "#5555FF",  "#55FF55",  "#55FFFF",  "#FF5555",  "#FF55FF",  "#FFFF55",  "#FFFFFF"};


    //convert to pallete
    public BufferedImage convertToPallete(BufferedImage img, String pallete[]){

        // create new empty image with same width and height as the parameter image
        BufferedImage out = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);

        //walk through all image pixels

        for (int y = 0; y < img.getHeight(); y++) {

            for (int x = 0; x < img.getWidth(); x++) {

                float minProximity = 255; //create a float to be the minimum proximity

                Color currentPixelColor = new Color(img.getRGB(x, y)); //create a color to get the current pixel color
                Color newColorInPixel = currentPixelColor; // create another color to be the new one

                //walk through all the colors of the pallete
                for (int i = 0; i <pallete.length; i ++){

                    Color colorInTable = Color.decode(pallete[i]);//create a color with the hex string in the pallete

                    int r1, g1, b1; // create 6 variables for 1 and 2 colors RGB
                    int r2, g2, b2;


                    // assign values for the currentColor

                    r1 = currentPixelColor.getRed();
                    g1 = currentPixelColor.getGreen();
                    b1 = currentPixelColor.getBlue();

                    // assign values for the color in the palette

                    r2 = colorInTable.getRed();
                    g2 = colorInTable.getGreen();
                    b2 = colorInTable.getBlue();

                    //take average RGB

                    float average = (Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2))/3;

                    //check if average is less than current minProximity


                    if(average < minProximity){

                        //if it is, the new minProximity is the current average

                        minProximity = average;

                        //and the new color in pixel is the current table color

                        newColorInPixel = colorInTable;
                    }

                }

                //set the newcolor RGB
                int newRGB = newColorInPixel.getRGB();

                out.setRGB(x, y, newRGB);

            }
        }
        return out;

    }



    public void run() throws IOException {

        BufferedImage img = ImageIO.read(new File("src/puppy.png"));

        BufferedImage puppy64 = convertToPallete(img, colors);

        ImageIO.write(puppy64, "png",
                new File("puppy64.png"));

        System.out.println("Pronto!");
    }

    public static void main(String [] args) throws IOException {
        new Atividade1().run();
    }
}
